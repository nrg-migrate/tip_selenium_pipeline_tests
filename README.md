# Selenium Tests for NRG Systems #

## Usage ##
Change local.properties.sample to local.properties and change the information as necessary. Run the tests using "mvn clean test -Dargument1=x -Dargument2=y ..." Some additional information can be found at https://wiki.xnat.org/display/XNATDev/Selenium+Tests . NRG users can request the password for the selenium gmail account from Charlie Moore .

## Test Information ##
Information describing what tests are executed by this project, along with any other relevant project-specific information can be found in another file in this directory.

### XNAT Command Line Arguments ###
Any arguments marked Optional* must be specified in the XNAT .properties file if not set here.

* xnat.selenium.user: (Optional*) - The username of a non-admin account. This account need not exist.
* xnat.selenium.password: (Optional*) - The password of a non-admin account. This account need not exist.
* xnat.seleniumAdmin.user: (Optional*) - The username of an admin account. This account need not exist.
* xnat.seleniumAdmin.password: (Optional*) - The password of an admin account. This account need not exist.
* xnat.admin.user: (Optional*) - The username of an admin account. This account must exist and is used to create the selenium accounts.
* xnat.admin.password: (Optional*) - The password of an admin account. This account must exist and is used to create the selenium accounts.
* xnat.users.email: (Optional*) - The email address for the selenium user accounts. This must be a gmail account, preferably with at least 10 characters.
* xnat.users.email.password: (Optional*) - The password for the email address for the selenium user accounts
* test (Optional) - The class name of a test class. Provide this if you only want to test one class.
* xnat.config (Optional) - The name of the properties file (extension included). If this parameter is not provided, it will default to local.properties.
* xnat.runAll (Optional) - Parameter which determines if all tests should be run. Use false to skip lengthy tests, or use value true to run all tests. If the parameter is not provided, it will default to false.
* xnat.baseurl: (Optional*) - The URL for the test environment
* xnat.defaultTimeout: (Optional) - Timeout in seconds for certain Selenium operations. Do not change from 30 without a reason. Defaults to 30.
* xnat.browser: (Optional) - Browser to run the tests in, which must be installed on your system. Currently supports Firefox and (Windows) Chrome. Defaults to Firefox.
* xnat.init: (Optional) - Should selenium XNAT accounts be set up? Useful to turn off to speed up debugging, but should not be used for running tests. Defaults to true.
* xnat.dicom.host: (Optional) - Host for XNAT DICOM Receiver. Relevant tests will be skipped if this is not provided here or in properties.
* xnat.dicom.port: (Optional) - Port number for XNAT DICOM Receiver. Relevant tests will be skipped if this is not provided here or in properties.
* xnat.dicom.aetitle: (Optional) - DICOM AE Title for XNAT DICOM Receiver. Relevant tests will be skipped if this is not provided here or in properties.
* xnat.jira: (Optional) - Should a cycle be created in JIRA according to jira.properties? Defaults to false.
* xnat.requireAdmin: (Optional) - Are admin accounts required/allowed? Defaults to true.
* selenium.version (Optional) - Version of the Selenium JAR to use. Defaults to the version specified in the selenium_dependencies parent POM if not provided.

### XNAT Properties File ###
The following attributes can be changed in the .properties file OR overwritten as Command Line Arguments. Any attributes not marked Optional must be set either here, or as Command Line Arguments.

* xnat.selenium.user - The username of a non-admin account. This account need not exist.
* xnat.selenium.password - The password of a non-admin account. This account need not exist.
* xnat.seleniumAdmin.user - The username of an admin account. This account need not exist.
* xnat.seleniumAdmin.password - The password of an admin account. This account need not exist.
* xnat.admin.user - The username of an admin account. This account must exist and is used to create the selenium accounts.
* xnat.admin.password - The password of an admin account. This account must exist and is used to create the selenium accounts.
* xnat.users.email - The email address for the selenium user accounts
* xnat.users.email.password - The password for the email address for the selenium user accounts
* xnat.baseurl - The URL for the test environment
* xnat.defaultTimeout: (Optional) - Timeout in seconds for certain Selenium operations. Do not change from 30 without a reason. Defaults to 30.
* xnat.runAll (Optional) - Parameter which determines if all tests should be run. Use false to skip lengthy tests, or use value true to run all tests. If the parameter is not provided, it will default to false.
* xnat.browser: (Optional) - Browser to run the tests in, which must be installed on your system. Currently supports Firefox and (Windows) Chrome. Defaults to Firefox.
* xnat.init: (Optional) - Should selenium XNAT accounts be set up? Useful to turn off to speed up debugging, but should not be used for running tests. Defaults to true.
* xnat.dicom.host: (Optional) - Host for XNAT DICOM Receiver. Relevant tests will be skipped if this is not provided here or as a command line argument.
* xnat.dicom.port: (Optional) - Port number for XNAT DICOM Receiver. Relevant tests will be skipped if this is not provided here or as a command line argument.
* xnat.dicom.aetitle: (Optional) - DICOM AE Title for XNAT DICOM Receiver. Relevant tests will be skipped if this is not provided here or as a command line argument.
* xnat.jira: (Optional) - Should a cycle be created in JIRA according to jira.properties? Defaults to false.
* xnat.requireAdmin: (Optional) - Are admin accounts required/allowed? Defaults to true.

### JIRA Command Line Arguments ###
All fields are required if you wish to use JIRA integration. They may be set as command line arguments or in config/jira.properties.

* jira.user - The username of the JIRA account used to execute tests.
* jira.password: The password of the above account.
* jira.url: URL for JIRA.
* jira.project: Project key for project that contains the tests.
* jira.project.version: Version under which to file the cycle.

### Full Example ###
A full example of running this given by using the included local.properties.sample renamed to local.properties with command line launch "mvn clean test -Dxnat.config=local.properties -Dxnat.selenium.password=password -Dxnat.seleniumAdmin.password=password -Dxnat.admin.password=password -Dxnat.users.email.password=password"

### Screenshots ###
All existing screenshots will be deleted when running the test suite again, so make sure to back them up somewhere if you do not want to lose them.

#### Failing Tests ####
Although screenshots taken at certain points of tests may be unhelpful, a screenshot will be taken on each test failure. These screenshots are placed in target/Failed_Test_Screenshots/{class name}.

#### Important Steps ####
Screenshots of important steps of tests are placed in target/Test_Step_Screenshots/{class name}.

## Browser Support ##
As of Version 1.2, the test suite should work spottily with Chrome, in addition to Firefox. Version 1.3 additionally includes extremely spotty support for IE. Chrome seems to be significantly faster than Firefox, but has major issues on CentOS, so the default browser remains Firefox. For IE, the configuration available at https://code.google.com/p/selenium/wiki/InternetExplorerDriver#Required_Configuration must be set up.

## Jenkins ##
If you wish to use this test suite with jenkins, install the Xvfb plugin. Under build environment, Xvfb screen should be set to at least 1280x1024x24.

## JIRA Integration ##
As of Version 3.0, the test suite will create a cycle with executions for tests executed in JIRA as long as xnat.jira is set to true. Failure screenshots will be attached to failing tests along with a stack trace.