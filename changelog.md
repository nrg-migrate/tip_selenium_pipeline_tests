# Changelog #
## Version 1.0 ##
* Initial Release
## Version 1.1 ##
* Added test execution time logging.
* Added several more tests.
* Fixed issue where multiple WebDriver objects were created.
* Fixed screenshot naming.
* Basic support for running a more compact version versus all of the tests (runAll flag).
* Various other assorted fixes.
## Version 1.2 ##
* Support added for Chrome.
* Added basic statistics (mean, median, "trend") to time logs.
* Fixed issue where loading Administer > Users was flaky.
* Various other assorted fixes.
## Version 1.3 ##
* Significant code cleanup.
* Time logs and screenshots should now work correctly when a test fails in setup.
* Added test to ensure project creation can be limited to admins.
* Spotty IE support.
## Version 2.0 ##
* Added a test to ensure that issue reports sent through XNAT are working and reaching the admin.
* Added several tests to test scripting UI and functionality.
* Integrated REST and dcm4che and added Prearchive testing code to add DICOM Sending tests.
* Added several tests to verify correct PET-MR functionality.
* Fixed a rare issue which failed on user creation.
## Version 3.0 ##
* XNAT JIRA integration
* Switch to RestAssured from HttpClient
* Migrated code from BaseTestCase to SeleniumUtils
* Added a test for system-wide alerts
## Version 4.0 ##
* Massive internal refactor for element locators
* Added tests for Anonymization Scripts
* Added tests for Series Import Filters
* Added tests for Scripting with site-level triggers
* Added tests for Scripting using Python
* Fixed issue where tests failing in @Before were not added to JIRA cycle
## Version 4.1 ##
* Additional code cleanup
* Added a test for script queueing
* Updated SQL test for alias SQL injection
* Moved lots of code into nrg_selenium
* Fixed issue with email addresses and consecutive periods
## Version 5.0 ##
* Switched testing framework from JUnit to TestNG