/*
 * org.nrg.tip.selenium.tests.TestTIP_Pipelines
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.tip.selenium.tests;

import org.nrg.selenium.AssortedUtils;
import org.nrg.selenium.pipeline.BasePipelineTest;
import org.nrg.selenium.Settings;
import org.nrg.selenium.pipeline.CSVGenerator;
import org.nrg.selenium.pipeline.configElements.*;
import org.nrg.selenium.xnat.XNATLocators;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.File;

public class TestTIP_Pipelines extends BasePipelineTest {

    private static final String BENICE_SESSION = "TIP_EXXXX";

    private static final String BENICE_LAUNCH_NAME = "Benice";

    private static final String BENICE_HISTORY_NAME = "Benice";

    @BeforeSuite
    public void checkAccount() {
        AssortedUtils.checkAccount(Settings.seleniumCredentials(), "Selenium");
    }

    @Test()
    public void testLaunchBenice() {
        config.add(new SelectElement(By.xpath("//select[@name='mprs']"), "2 (SAG T1 MPR)"));
        config.add(new ClickElement(By.xpath("//input[@value='5' and @type='checkbox']")));
        config.add(new ClickElement(By.xpath("//input[@value='11' and @type='checkbox']")));
        config.add(new ClickElement(XNATLocators.SHOW_ADVANCED_PARAMS));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("TR_slc"), "0"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("sorted"), "1"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("economy"), "5"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("normode"), "0"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("skip"), "1"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("imaflip"), "0"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("epi2atl"), "1"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("Siemens_interleave"), "1"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("epidir"), "0"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("qc_to_dicom_flag"), "1"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("Gad"), "0"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("copyProcessed"), "0"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("go"), "1"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("copyRaw"), "1"));
        config.add(new FillElement(XNATLocators.PIPELINE_INPUT_PARAM("copyAssessors"), "0"));
        config.add(new ClickElement(XNATLocators.BUTTON_SUBMIT));
        config.add(new ClickElement(XNATLocators.OK_CLOSE_BUTTON));
        config.add(new ScreenshotElement(By.xpath("//div[contains(text(),'pipeline successfully queued.')]")));
        config.add(new SimpleClickElement(XNATLocators.OK_CLOSE_BUTTON));
        performPipelineTest(BENICE_SESSION, BENICE_LAUNCH_NAME, BENICE_HISTORY_NAME);
    }

}
